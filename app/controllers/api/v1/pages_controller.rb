class Api::V1::PagesController < Api::V1::ApplicationController

  # GET /pages
  def index
    @pages = Page.all

    render(json: @pages)
  end

  # POST /pages/parse
  def parse
    @page = Page.find_or_create_by(url: params[:url].strip)

    PageAnalyzer.new(@page, %w(h1 h2 h3 a)).parse

    if @page.errors.empty?
      render(json: @page)
    else
      render(json: @page.errors, status: :unprocessable_entity)
    end
  end

  private

  # Only allow a trusted parameter "white list" through.
  def page_params
    params.require(:page).permit(:url)
  end
end
