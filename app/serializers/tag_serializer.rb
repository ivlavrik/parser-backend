class TagSerializer < ActiveModel::Serializer
  attributes :id, :key, :value, :tag_attributes
  belongs_to :page
  has_many :tag_attributes
end
