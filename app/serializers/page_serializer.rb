class PageSerializer < ActiveModel::Serializer
  attributes :id, :url, :tags
  has_many :tags
end
