class TagAttributeSerializer < ActiveModel::Serializer
  attributes :id, :key, :value
  belongs_to :tag
end
