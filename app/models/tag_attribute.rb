class TagAttribute < ApplicationRecord
  belongs_to :tag

  validates_presence_of :tag, :key
end
