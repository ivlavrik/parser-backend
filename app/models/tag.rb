class Tag < ApplicationRecord
  belongs_to :page
  has_many :tag_attributes, dependent: :destroy

  validates_presence_of :page, :key
end
