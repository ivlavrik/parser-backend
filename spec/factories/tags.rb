FactoryGirl.define do
  factory :tag do
    key { Faker::Lorem.word }
    value { Faker::Lorem.sentence }
    page

    trait :with_tag_attributes do
      after(:create) do |tag|
        create :tag_attribute, tag: tag
      end
    end
  end
end
