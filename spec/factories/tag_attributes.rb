FactoryGirl.define do
  factory :tag_attribute do
    key 'href'
    value { Faker::Internet.url }
    tag
  end
end
