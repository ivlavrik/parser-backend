FactoryGirl.define do
  factory :page do
    url { Faker::Internet.url }

    trait :with_tags do
      after(:create) do |page|
        create :tag, page: page, key: 'h1'
        create :tag, page: page, key: 'h2'
        create :tag, page: page, key: 'h3'
        create :tag, :with_tag_attributes, page: page, key: 'a'
      end
    end
  end
end
