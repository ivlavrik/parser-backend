require 'rails_helper'

RSpec.describe TagAttributeSerializer, :type => :serializer do
  let(:tag_attribute) { create :tag_attribute }

  subject { serialize(tag_attribute) }

  describe "attributes" do
    it "include valid key" do
      expect(JSON.parse(subject)['key']).to eq tag_attribute.key
    end

    it "include valid value" do
      expect(JSON.parse(subject)['value']).to eq tag_attribute.value
    end

    it "include valid tag" do
      expect(JSON.parse(subject)['tag']['id']).to eq tag_attribute.tag.id
    end
  end
end
