require 'rails_helper'

RSpec.describe PageSerializer, :type => :serializer do
  let(:page) { create :page, :with_tags }

  subject { serialize(page) }

  describe "attributes" do
    it "include valid url" do
      expect(JSON.parse(subject)['url']).to eq page.url
    end

    it "include page tags" do
      expect(JSON.parse(subject)['tags'].count).to eq page.tags.count
    end
  end
end
