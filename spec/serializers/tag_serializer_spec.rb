require 'rails_helper'

RSpec.describe TagSerializer, :type => :serializer do
  let(:tag) { create :tag, :with_tag_attributes }

  subject { serialize(tag) }

  describe "attributes" do
    it "include valid key" do
      expect(JSON.parse(subject)['key']).to eq tag.key
    end

    it "include valid value" do
      expect(JSON.parse(subject)['value']).to eq tag.value
    end

    it "include valid page url" do
      expect(JSON.parse(subject)['page']['url']).to eq tag.page.url
    end

    it "include page tags" do
      expect(JSON.parse(subject)['tag_attributes'].count).to eq tag.tag_attributes.count
    end
  end
end
