require 'rails_helper'

RSpec.describe Api::V1::PagesController, type: :controller do
  let!(:page) { create :page }
  let!(:html_content) { File.read('spec/assets/page.html') }
  let(:url) { Faker::Internet.url }

  describe "GET #index" do
    it "assigns all pages as @pages" do
      get :index
      expect(assigns(:pages)).to eq([page])
    end
  end

  describe "POST #parse" do
    context "alive page" do
      subject { post :parse, params: { url: url } }

      before do
        allow_any_instance_of(PageAnalyzer).to receive(:open).and_return html_content
      end

      it "assigns page as @page" do
        subject
        expect(assigns(:page)).to eq(Page.find_by_url(url))
      end

      %w(id url tags).each do |attr|
        it "response contain #{attr} attribute" do
          subject
          expect(JSON.parse(response.body)).to have_key(attr)
        end
      end

      context "with new url" do
        it "creates a new Page" do
          expect { subject } .to change(Page, :count).by(1)
        end
      end

      context "with existing url in database" do
        subject { post :parse, params: { url: page.url } }

        it "doesn't create a new Page" do
          expect { subject } .not_to change(Page, :count)
        end
      end
    end
  end

  context "non-responding page" do
    subject { response }
    before { post :parse, params: { url: url } }

    it { expect(subject.status).to eq 422 }
  end
end
