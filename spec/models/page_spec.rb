require 'rails_helper'

RSpec.describe Page, type: :model do
  subject { create :page }

  describe "associations" do
    it "has many tags" do
      assc = Page.reflect_on_association(:tags)
      expect(assc.macro).to eq :has_many
    end
  end

  describe "validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without an url" do
      subject.url = nil
      expect(subject).to_not be_valid
    end
  end
end
