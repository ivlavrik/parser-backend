require 'rails_helper'

RSpec.describe Tag, type: :model do
  subject { create :tag }

  describe "associations" do
    it "belongs to page" do
      assc = Tag.reflect_on_association(:page)
      expect(assc.macro).to eq :belongs_to
    end

    it "has many tag attributes" do
      assc = Tag.reflect_on_association(:tag_attributes)
      expect(assc.macro).to eq :has_many
    end
  end

  describe "validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without a page" do
      subject.page = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without a key" do
      subject.key = nil
      expect(subject).to_not be_valid
    end
  end
end
