require 'rails_helper'

RSpec.describe TagAttribute, type: :model do
  subject { create :tag_attribute }

  describe "associations" do
    it "belongs to tag" do
      assc = TagAttribute.reflect_on_association(:tag)
      expect(assc.macro).to eq :belongs_to
    end
  end

  describe "validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without a tag" do
      subject.tag = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without a key" do
      subject.key = nil
      expect(subject).to_not be_valid
    end
  end
end
