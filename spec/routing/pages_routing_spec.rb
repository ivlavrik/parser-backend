require "rails_helper"

RSpec.describe Api::V1::PagesController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/api/v1/pages").to route_to("api/v1/pages#index")
    end

    it "routes to #parse" do
      expect(:post => "/api/v1/pages/parse").to route_to("api/v1/pages#parse")
    end
  end
end
