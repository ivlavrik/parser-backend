require 'rails_helper'

RSpec.describe PageAnalyzer, type: :lib do
  let(:page) { create :page }
  let!(:html_content) { File.read('spec/assets/page.html') }

  describe "#parse" do
    subject { page.tags }

    before do
      allow_any_instance_of(PageAnalyzer).to receive(:open).and_return html_content
      PageAnalyzer.new(page, %w(h1 h2 h3 a)).parse
    end

    %w(h1 a).each do |attr|
      it "stores only one '#{attr}' tag" do
        expect(subject.where(key: attr).count).to eq 1
      end
    end

    it "stores 'h1' with valid value" do
      expect(subject.where(key: 'h1').last.value).to eq 'Example Domain'
    end

    it "stores 'a' tag with valid value" do
      expect(subject.where(key: 'a').last.value).to eq 'More information...'
    end

    context "tag with attributes" do
      subject { page.tags.where(key: 'a').last.tag_attributes }

      it "stores only one 'href' attribute for 'a' tag" do
        expect(subject.where(key: 'href').count).to eq 1
      end

      it "stores 'href' attribute for 'a' tag with valid value" do
        expect(subject.where(key: 'href').last.value).to eq 'http://www.iana.org/domains/example'
      end
    end
  end
end
