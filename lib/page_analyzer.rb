require 'open-uri'

class PageAnalyzer
  def initialize(page, tag_names)
    @page, @tag_names = page, tag_names
  end

  def parse
    html = open(@page.url)
    data = Nokogiri::HTML(html).css(*@tag_names)

    @page.tags.destroy_all
    data.each { |el| save_tag(el) }
  rescue
    @page.errors.add(:parse, I18n.t('errors.parse', url: @page.url))
  end

  private

  def tag_attribute_names
    { 'a' => 'href' }
  end

  def save_tag(element)
    @page.tags.create(key: element.name, value: element.text).tap do |tag|
      attr_names = *tag_attribute_names[tag.key]
      attr_names.each do |attr|
        attribute = element.attribute(attr)
        tag.tag_attributes.create(
          key: attribute.name,
          value: attribute.value
        )
      end
    end
  end
end
