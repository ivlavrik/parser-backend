# parser-frontend

## Setting up

    install ruby 2.3.3 (recommended usage of rvm)
    bundle install
    rake db:create
    rails s -p 5000

- important to run rails server with 5000 port if you want to use it with frontend application (see instructions in correspondent repository)
