Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      resources :pages, only: [:index] do
        post :parse, on: :collection
      end
    end
  end
end
