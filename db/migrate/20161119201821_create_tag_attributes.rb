class CreateTagAttributes < ActiveRecord::Migration[5.0]
  def change
    create_table :tag_attributes do |t|
      t.string :key
      t.text :value
      t.references :tag, foreign_key: true

      t.timestamps
    end
  end
end
